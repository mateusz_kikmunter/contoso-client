# About
Old EF Core and MVC tutorial rewritten to .NET Core WebApi + JWT authentication.

https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/?view=aspnetcore-2.0

This is front-end application.

Web Api part - https://bitbucket.org/mateusz_kikmunter/contoso-api/src/master/

Identity Server part - https://bitbucket.org/mateusz_kikmunter/contoso-api-auth-server/src/master/
