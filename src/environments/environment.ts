
export const environment = {
  production: false,
  openIdConnectionSettings: {
    authority: "https://localhost:44331/",
    client_id: "university_spa",
    redirect_uri: "https://localhost:4200/login",
    scope: "openid profile email university-api",
    response_type: "id_token token",
    post_logout_redirect_uri: "https://localhost:4200/welcome",
    automaticSilentRenew: true,
    loadUserInfo: true,
    filterProtocolClaims: true,
    silent_redirect_uri: "https://localhost:4200/silent-renew"
  }
};