import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { SilentRenewComponent } from './silent-renew/silent-renew.component';

const routes: Routes = [
    { path: "login", component: LoginComponent },
    { path: "silent-renew", component: SilentRenewComponent },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class AccountRoutingModule {}