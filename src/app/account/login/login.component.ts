import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './../../core/authentication/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

   ngOnInit() {
    this.authService.userAuthenticated$.subscribe(userLoaded => {
      if(userLoaded){
        this.router.navigateByUrl("welcome");
      }
    });

    this.authService.completeAuthentication();
  }
}
