import { AuthService } from './../../core/authentication/auth.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { LoginComponent } from './login.component';
import { SilentRenewComponent } from '../silent-renew/silent-renew.component';
import { MockRouter } from './../../shared/mocks/mock.router';
import { MockAuthService } from 'src/app/shared/mocks/mock-auth.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let mockAuthService: MockAuthService = new MockAuthService();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        LoginComponent,
        SilentRenewComponent
       ],
      providers: [ 
        { provide: Router, useValue: MockRouter },
        { provide: AuthService, useValue: mockAuthService },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
