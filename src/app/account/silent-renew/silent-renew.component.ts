import { AuthService } from './../../core/authentication/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-silent-renew',
  templateUrl: './silent-renew.component.html',
  styleUrls: ['./silent-renew.component.css']
})
export class SilentRenewComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.silentRenew();
  }

}
