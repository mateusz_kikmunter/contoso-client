import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account.routing-module';
import { LoginComponent } from './login/login.component';
import { SilentRenewComponent } from './silent-renew/silent-renew.component';


@NgModule({
  imports: [
    CommonModule,
    AccountRoutingModule,
  ],
  declarations: [
    LoginComponent, 
    SilentRenewComponent
  ]
})
export class AccountModule { }
