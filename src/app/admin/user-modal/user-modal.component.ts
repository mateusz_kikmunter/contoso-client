import { ActionResult } from './../../shared/enums/action.result';
import { Component, OnInit, Input } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Mode } from './../../shared/enums/mode';
import { UserForManipulation } from 'src/app/shared/models/user/user-for-manipulation.model';
import { UserService } from 'src/app/core/services/user.service';
import { Role } from 'src/app/shared/enums/role';
import { FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.css']
})
export class UserModalComponent implements OnInit {

  private roles: string[] = [];

  public userForm: FormGroup;

  get firstName(): AbstractControl {
    return this.userForm.get("firstName");
  }

  get lastName(): AbstractControl {
    return this.userForm.get("lastName");
  }

  get email(): AbstractControl {
    return this.userForm.get("email");
  }

  get password(): AbstractControl {
    return this.userForm.get("password");
  }

  get active(): AbstractControl {
    return this.userForm.get("active");
  }

  get role(): AbstractControl {
    return this.userForm.get("role");
  }

  @Input() user: UserForManipulation = new UserForManipulation();
  @Input() mode: Mode = Mode.READONLY;

  constructor(
    public modal: NgbActiveModal,
    private userService: UserService) { }

  ngOnInit() {
    this.populateRolesDropdown();

    this.userForm = new FormGroup({
      firstName: new FormControl({ value: this.getControlValue("firstName"), disabled: this.isReadOnlyMode() }, [Validators.required, Validators.maxLength(255)]),
      lastName: new FormControl({ value: this.getControlValue("lastName"), disabled: this.isReadOnlyMode() }, [Validators.required, Validators.maxLength(255)]),
      email: new FormControl({ value: this.getControlValue("email"), disabled: this.isReadOnlyMode() }, [Validators.required, Validators.email]),
      active: new FormControl({ value: this.getControlValue("active"), disabled: this.isReadOnlyMode() }),
      role: new FormControl({ value: this.getControlValue("role"), disabled: this.isReadOnlyMode() }, Validators.required),
      password: new FormControl({ value: this.getControlValue("password")}, [Validators.required, Validators.minLength(6)])
    });
  }

  private getControlValue(property: string): any {
    return this.user ? this.user[property] : "";
  }

  private saveUser(): void {

    const user: UserForManipulation = new UserForManipulation(this.userForm.value);

    if (this.mode === Mode.ADD) {
      this.userService.registerUser(user).subscribe(user =>
        this.modal.close(ActionResult.SUCCESS),
        error => console.log(error));
    }

    if(this.mode === Mode.EDIT) {
      //TODO create update method on user service and call it here
      
    }
  }

  private populateRolesDropdown(): void {
    if (this.roles.length === 0) {
      this.roles = Object.keys(Role).map(k => k);
    }
  }

  private isAddMode(): boolean {
    return this.mode === Mode.ADD;
  }

  private isReadOnlyMode(): boolean {
    return this.mode === Mode.READONLY;
  }

  private getModalTitle(): string {
    return this.mode === Mode.READONLY
      ? `${this.user.firstName} ${this.user.lastName}`
      : this.mode;
  }
}