import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManipulateStudentComponent } from '../manipulate-student/manipulate-student.component';
import { StudentsComponent } from './students.component';
import { UserService } from 'src/app/core/services/user.service';

describe('UsersComponent', () => {
  let component: StudentsComponent;
  let fixture: ComponentFixture<StudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        ManipulateStudentComponent,
        StudentsComponent
       ],
       imports: [
         FormsModule,
         HttpClientModule
       ],
       providers: [
         UserService
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
