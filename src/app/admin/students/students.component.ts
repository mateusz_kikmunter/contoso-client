// Angular imports
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';

// 3rd party imports
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { faCheck, faTimes, IconDefinition } from '@fortawesome/free-solid-svg-icons';

// application imports
import { ApplicationUser } from './../../shared/models/user/application-user.model';
import { ActionResult } from './../../shared/enums/action.result';
import { Mode } from './../../shared/enums/mode';
import { UserModalComponent } from '../user-modal/user-modal.component';
import { UserForManipulation } from 'src/app/shared/models/user/user-for-manipulation.model';
import { UserService } from './../../core/services/user.service';
import { DataTablesSelect } from 'src/app/core/services/select.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit, OnDestroy {

  private users: ApplicationUser[] = [];
  private dtOptions: any = {};
  private dtTrigger = new Subject();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  private userActiveIcon: IconDefinition = faCheck;
  private userDisabledIcon: IconDefinition = faTimes;

  constructor(
    private select: DataTablesSelect<ApplicationUser>,
    private modal: NgbModal,
    public userService: UserService,
    private spinner: NgxUiLoaderService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      select: true
    };

    this.spinner.start();

    this.userService.getDataTableData().subscribe(users => {
        this.users = users;
        this.dtTrigger.next();
        this.spinner.stop();
      });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.spinner.start();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.clear();
      dtInstance.destroy();
      this.select.clearSelection();
      // Call the dtTrigger to rerender again
      this.userService.getDataTableData().subscribe(users => {
        this.users = users;
        this.dtTrigger.next();
        this.spinner.stop();
      });
    });
  }

  public handleButtonClick(mode: string): void {
    this.actionDispatcher(mode, this.select.selectedItem);
  }

  private openModal(mode: Mode, user: ApplicationUser): void {
    const modalReference = this.modal.open(UserModalComponent);
    modalReference.componentInstance.mode = mode;
    modalReference.componentInstance.user = (!user || mode === Mode.ADD) ? new UserForManipulation() : user;

    modalReference.result.then(result => {
      if (result === ActionResult.SUCCESS) {
        this.select.clearSelection();
        this.rerender();
      }
    },
      result => this.onAlternativeModalClose(result));
  }

  private onAlternativeModalClose(reason: any): any {
    return reason === ModalDismissReasons.ESC || reason === ModalDismissReasons.BACKDROP_CLICK ? {} : alert(`Modal closed with ${reason}`);
  }

  private renderUserActive(isUserActive: boolean): IconDefinition {
    return isUserActive ? this.userActiveIcon : this.userDisabledIcon;
  }

  private actionDispatcher = (mode: string, user: ApplicationUser) => ({
    "Add": () =>    this.openModal(Mode.ADD, undefined),
    "Edit": () =>   this.openModal(Mode.EDIT, user),
    "ReadOnly": () =>   this.openModal(Mode.READONLY, user)
  })[mode]()
}
