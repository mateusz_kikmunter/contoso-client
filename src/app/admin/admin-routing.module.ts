import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { AdminGuardService } from '../core/authentication/admin-guard.service';
import { StudentsComponent } from "./students/students.component";

const adminRoutes: Routes = [
    { path: "users", component: StudentsComponent, canActivate: [AdminGuardService] }
];

@NgModule({
    imports: [
        RouterModule.forChild(adminRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule { }