//Angular imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//3rd party imports
import { DataTablesModule } from 'angular-datatables';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

//application imports
import { AdminRoutingModule } from './admin-routing.module';
import { StudentsComponent } from './students/students.component';
import { UserModalComponent } from './user-modal/user-modal.component';

@NgModule({
  imports: [
    AdminRoutingModule,
    CommonModule,
    DataTablesModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule
  ],
  declarations: [
    StudentsComponent,
    UserModalComponent
  ],
  entryComponents: [
    UserModalComponent
  ]
})
export class AdminModule { }
