import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from './core/authentication/auth-guard.service';
import { WelcomeComponent } from './welcome/welcome.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
    { path: "", component: HomeComponent, canActivate: [ AuthGuardService ] },
    { path: "home", component: HomeComponent, canActivate: [ AuthGuardService ] },
    { path: "welcome", component: WelcomeComponent },
    { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
    { path: "**", redirectTo: "", pathMatch: "full" }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class ApplicationRoutingModule {}