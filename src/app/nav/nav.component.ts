import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { AuthService } from './../core/authentication/auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit, OnDestroy {

  private menuCollapsed: boolean = true;
  private authenticated: boolean;
  private subscription: Subscription;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.subscription = this.authService.userAuthenticated$.subscribe(status => this.authenticated = status);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  toggleMenu(): void {
    this.menuCollapsed = !this.menuCollapsed;
  }

  logout(): void {
    this.authService.logout();
  }

  login(): void {
    this.authService.login();
  }

}
