import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { ApplicationRoutingModule } from './../application.routing-module';

import { HomeComponent } from './../home/home.component';
import { NavComponent } from './nav.component';
import { WelcomeComponent } from './../welcome/welcome.component';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        NavComponent,
        WelcomeComponent
      ],
      imports: [
        ApplicationRoutingModule,
        NgbCollapseModule.forRoot()
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
