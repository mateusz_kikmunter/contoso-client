import { BehaviorSubject } from "rxjs";

export class MockAuthService {

    public static get authTokenValue(): string {
        return "token abc";
    };

    private authenticated: boolean = false;
    public userAuthenticated$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    private _user: any;
    public get user(): any {
        return this._user;
    }

    constructor() { }

    public completeAuthentication(): void { };
    public login(): void { };

    public setAuthenticationState(authenticated: boolean): void {
        this.authenticated = authenticated;
    }

    public setUser(user: any): void {
        this._user = user;
    }

    public isAuthenticated(): boolean {
        return this.authenticated;
    }
}