export enum Mode {
    READONLY = "ReadOnly",
    ADD = "Add",
    EDIT = "Edit"
}