export enum Role {
    Admin = "Admin",
    Instructor = "Instructor",
    Student = "Student"
}