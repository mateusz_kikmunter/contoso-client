export enum ActionResult {
    SUCCESS = "success",
    FAILURE = "failure"
}