import { Course } from './course/course';

export class Department {
    id: string;
    name: string;
    courses: Course[] = [];
}