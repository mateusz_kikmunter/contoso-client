export abstract class User {
    firstName: string;
    lastName: string;
    email: string;
    active: boolean;
}