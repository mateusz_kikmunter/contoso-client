import { Role } from "../../enums/role";
import { User } from "./user";

export class UserForManipulation extends User {
    password: string;
    role: Role = Role.Student;

    constructor(init?: Partial<UserForManipulation>) {
        super();
        Object.assign(this, init);
    }
}