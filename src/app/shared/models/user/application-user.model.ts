import { User } from "./user";
import { Role } from "../../enums/role";

export class ApplicationUser extends User {
    id: string;
    role: Role;
}