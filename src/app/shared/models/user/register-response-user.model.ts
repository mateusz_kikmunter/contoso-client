import { User } from './user';

export class RegisterResponseUser extends User {
    id: string;
}