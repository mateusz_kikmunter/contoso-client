import { Department } from "./department";
import { User } from "./user/user";

export class Instructor extends User {
    id: string;
    authenticationId: string;
    fullName: string;
    department: Department;
}