import { CourseStatus } from "./course.status";
import { Instructor } from "../instructor";
import { Enrollment } from "../enrollment";
import { Department } from "../department";

export class Course {
    id: string;
    title: string;
    status: CourseStatus;
    instructor: Instructor;
    department: Department;
    enrollments: Enrollment[] = [];
}