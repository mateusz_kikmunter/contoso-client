import { Student } from './student/student';
import { Grade } from '../enums/grade';
import { Course } from './course/course';

export class Enrollment {
    id: string;
    grade: Grade;
    course: Course;
    student: Student;
}