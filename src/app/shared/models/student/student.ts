import { RegisterStudentModel } from './register-student.model';
import { Enrollment } from '../enrollment';

export class Student extends RegisterStudentModel {
    id: string;
    fullName: string;
    enrollments: Enrollment[] = [];
}