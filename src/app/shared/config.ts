export class Config {

    public static universityApiUrl: string = 'https://localhost:44342/api';
    
    public static universityAuthServer: string = 'https://localhost:44331';
}