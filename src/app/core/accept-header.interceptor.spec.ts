import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, inject } from '@angular/core/testing';

import { AcceptHeaderInterceptor } from './accept-header.interceptor';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

describe('AcceptHeader.InterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
    providers: [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: AcceptHeaderInterceptor,
        multi: true
      }
    ]
  }));

  it('should be created', () => {
    const service: AcceptHeaderInterceptor = TestBed.get(AcceptHeaderInterceptor);
    expect(service).toBeTruthy();
  });

  it('should add accept header to the request', inject([HttpClient, HttpTestingController], (httpClient: HttpClient, http: HttpTestingController) => {
    const uri = 'api/fake/';
    const acceptHeader = 'Accept';

    httpClient.get(uri).subscribe();

    const req = http.expectOne(uri);

    expect(req.request.method).toBe('GET');
    expect(req.request.headers.has(acceptHeader)).toBeTruthy();
    expect(req.request.headers.get(acceptHeader)).toBe('application/json');

    http.verify();
  }));
});
