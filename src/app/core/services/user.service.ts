import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ApplicationUser } from './../../shared/models/user/application-user.model';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Student } from '../../shared/models/student/student';
import { RegisterStudentModel } from '../../shared/models/student/register-student.model';
import { UserForManipulation } from '../../shared/models/user/user-for-manipulation.model';
import { Config } from '../../shared/config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public registerUser(user: UserForManipulation): Observable<Student> {
    return this.http.post<ApplicationUser>(`${Config.universityAuthServer}/api/account/register`, user)
      .pipe(
        switchMap(user => {

          const student: RegisterStudentModel = {
            firstName: user.firstName,
            lastName: user.lastName,
            active: user.active,
            email: user.email,
            authenticationId: user.id
          };

          return this.http.post<Student>(`${Config.universityApiUrl}/student`, student);
        })
      );
  }

  public getDataTableData(): Observable<ApplicationUser[]> {
    return this.http.get<ApplicationUser[]>(`${Config.universityAuthServer}/api/user/get-users`);
  }
}
