import { TestBed } from '@angular/core/testing';

import { DataTablesSelect } from './select.service';

describe('SelectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataTablesSelect<any> = TestBed.get(DataTablesSelect);
    expect(service).toBeTruthy();
  });
});
