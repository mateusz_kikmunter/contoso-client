import { Injectable } from '@angular/core';

import { isEqual } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class DataTablesSelect<T> {

  private _selectedItem: T = undefined;
  get selectedItem(): T { return this._selectedItem };

  constructor() { }

  public clearSelection(): void {
    this._selectedItem = null;
  }

  public selectRow(item: T): void {
    this._selectedItem = this.rowSelected(item) ? undefined : item;
  }

  private rowSelected(item: T): boolean {
    if (this._selectedItem && item) {
      return isEqual(item, this._selectedItem);
    }
  }
}