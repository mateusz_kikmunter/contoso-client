import { MockAuthService } from 'src/app/shared/mocks/mock-auth.service';
import { AuthService } from './auth.service';
import { TestBed } from '@angular/core/testing';

import { AdminGuardService } from './admin-guard.service';
import { Role } from '../../shared/enums/role';

describe('AdminGuardService', () => {
  const mockAuthService = new MockAuthService();

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: AuthService, useValue: mockAuthService }
    ]
  }));

  it('should be created', () => {
    const service: AdminGuardService = TestBed.get(AdminGuardService);
    expect(service).toBeTruthy();
  });

  it('should return false when user is not admin', () => {
    const service: AdminGuardService = TestBed.get(AdminGuardService);

    mockAuthService.setUser({ profile: { role: Role.Student } });
    mockAuthService.setAuthenticationState(true);

    expect(service.canActivate()).toBeFalsy();
  });

  it('should return false when user is not authenticated', () => {
    const service: AdminGuardService = TestBed.get(AdminGuardService);

    mockAuthService.setAuthenticationState(false);

    expect(service.canActivate()).toBeFalsy();
  });

  it('should return false when user is admin and not authenticated', () => {
    const service: AdminGuardService = TestBed.get(AdminGuardService);

    mockAuthService.setUser({ profile: { role: Role.Admin } });
    mockAuthService.setAuthenticationState(false);

    expect(service.canActivate()).toBeFalsy();
  });

  it('should return true when user is admin', () => {
    const service: AdminGuardService = TestBed.get(AdminGuardService);

    mockAuthService.setUser({ profile: { role: Role.Admin } });
    mockAuthService.setAuthenticationState(true);

    expect(service.canActivate()).toBeTruthy();
  });
});
