import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';

import { UserManager, User } from 'oidc-client';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUser: User | null;
  private manager: UserManager = new UserManager(environment.openIdConnectionSettings);

  private userLoaded$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public userAuthenticated$: Observable<boolean> = this.userLoaded$.asObservable();

  get user(): User {
    return this.currentUser;
  }

  get authTokenValue(): string {
    return `${this.user.token_type} ${this.user.access_token}`;
  }

  constructor() {
    this.manager.clearStaleState();

    this.manager.getUser().then(user => {
      this.currentUser = user;
      this.userLoaded$.next(this.isAuthenticated());
    });
  }

  public login(): Promise<any> {
    return this.manager.signinRedirect();
  }

  public logout(): Promise<any> {
    return this.manager.signoutRedirect();
  }

  public completeAuthentication(): void {
    this.manager.signinRedirectCallback()
      .then(user => {
        this.currentUser = user;
        this.userLoaded$.next(true);
      });
  }

  public isAuthenticated(): boolean {
    return this.user != null && !this.user.expired;
  }

  public silentRenew(): void {
    this.manager.signinSilentCallback().then(user => this.currentUser = user);
  }
}
