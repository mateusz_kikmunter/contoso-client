import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { TestBed, inject, async } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { AuthInterceptor } from './auth-interceptor';
import { MockAuthService } from 'src/app/shared/mocks/mock-auth.service';

describe('AuthInterceptorService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientTestingModule ],
    providers: [
      { provide: AuthService, useValue: MockAuthService },
      { provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
       }
    ]
  }));

  afterEach(inject([HttpTestingController], (http: HttpTestingController) => http.verify()));

  it('should be created', () => {
    const service: AuthInterceptor = TestBed.get(AuthInterceptor);
    expect(service).toBeTruthy();
  });

  it('should append authorization header to the request', async(inject([HttpClient, HttpTestingController], (httpClient: HttpClient, http: HttpTestingController) => {
    const uri: string = 'api/fake/1';
    const authHeader: string = 'Authorization'

    httpClient.get(uri).subscribe();
    const req = http.expectOne(uri);
    expect(req.request.url).toBe(uri);
    expect(req.request.method).toBe('GET');
    expect(req.request.headers.has(authHeader)).toBeTruthy();
    expect(req.request.headers.get(authHeader)).toBe(MockAuthService.authTokenValue);
  })));
});
