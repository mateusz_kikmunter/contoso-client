import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  canActivate(): boolean {
    if(this.authService.isAuthenticated()){
      return true;
    }
    else {
      this.authService.login();
      return false;
    }
  }

  constructor(private authService: AuthService, private router: Router) { }
}
