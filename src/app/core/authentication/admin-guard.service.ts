import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';

import { AuthService } from './auth.service';
import { Role } from '../../shared/enums/role';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardService implements CanActivate {

  canActivate(): boolean {
    if(this.authService.isAuthenticated() && this.authService.user.profile.role === Role.Admin){
      return true;
    }

    return false;
  }

  constructor(private authService: AuthService) { }
}
