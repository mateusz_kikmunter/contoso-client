import { Router } from '@angular/router';
import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import { MockRouter } from 'src/app/shared/mocks/mock.router';
import { AuthService } from './auth.service';
import { MockAuthService } from 'src/app/shared/mocks/mock-auth.service';

describe('AuthGuardService', () => {
  const mockAuthService: MockAuthService = new MockAuthService();

  beforeEach(() => TestBed.configureTestingModule({
     providers: [ 
       { provide: Router, useValue: MockRouter },
       { provide: AuthService, useValue: mockAuthService }
      ]
  }));

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });

  it('should return false when user not authenticated', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service.canActivate()).toBeFalsy();
  });

  it('should return true when user authenticated', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    mockAuthService.setAuthenticationState(true);
    expect(service.canActivate()).toBeTruthy();
  });
});