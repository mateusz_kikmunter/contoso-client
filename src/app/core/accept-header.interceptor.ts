import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AcceptHeaderInterceptor implements HttpInterceptor {

  private acceptHeader: string = "Accept"

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.headers.has(this.acceptHeader)) {
      request = request.clone({ headers: request.headers.set(this.acceptHeader, 'application/json') });
    }

    return next.handle(request);
  }

  constructor() { }
}
